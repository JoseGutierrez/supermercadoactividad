package com.example.supermercado.models;

public class UReponedor extends Usuario {
    private String seccion;

    public UReponedor(String nombre, String rut, String usuario, String contraseña, String seccion) {
        super(nombre, rut, usuario, contraseña);
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
