package com.example.supermercado.models;

public class UCajero extends Usuario{

    private String ventas;

    public UCajero(String nombre, String rut, String usuario, String contrasena, String ventas) {
        super(nombre, rut, usuario, contrasena);
        this.ventas = ventas;

    }

    public String getVentas() {
        return ventas;
    }

    public void setVentas(String ventas) {
        this.ventas = ventas;
    }
}
