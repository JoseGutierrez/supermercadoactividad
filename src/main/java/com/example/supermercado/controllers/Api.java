package com.example.supermercado.controllers;

import com.example.supermercado.models.Merma;
import com.example.supermercado.models.Producto;
import com.example.supermercado.models.UCajero;
import com.example.supermercado.models.UReponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
public class Api {
    @GetMapping("")
        public String sas() {
            return "miau";

    }
    @GetMapping("productos")
        public List<Producto> listaProductos(){
        Producto producto1 = new Producto("Tomate","Fruteria y Verduras",500,390,"Santa Isabel");
        Producto producto2 = new Producto("Leche embotellada","Leche",500,1390,"Santa Isabel");
        Producto producto3 = new Producto("Mantequilla surlat","Leche",120,550,"Santa Isabel");
        Producto producto4 = new Producto("Carne Mechada","Carnicería",600,2590,"Santa Isabel");
        Producto producto5 = new Producto("Coca cola 3L","Bebidas",800,2990,"Santa Isabel");
        Producto producto6 = new Producto("Huevos","Perecibles",522,190,"Santa Isabel");
        Producto producto7 = new Producto("Arroz juancho","No Perecibles",690,790,"Santa Isabel");
        Producto producto8 = new Producto("Manzanas","Fruteria y Verduras",400,990,"Santa Isabel");
        List<Producto> productoList = new ArrayList<>();
        productoList.add(producto1);
        productoList.add(producto2);
        productoList.add(producto3);
        productoList.add(producto4);
        productoList.add(producto5);
        productoList.add(producto6);
        productoList.add(producto7);
        productoList.add(producto8);
        return productoList;
    }
    @GetMapping("reponedor")
    public List<UReponedor> listaResponedores(){
        UReponedor reponedor1 = new UReponedor("Teodora Salas","19555642-5","tsalas","conejito","Verduras");
        UReponedor reponedor2 = new UReponedor("Javier Alexander Soto","20556387-k","jasoto","tiburon","Perecibles");
        UReponedor reponedor3 = new UReponedor("Yolanda Ruiz","20136387-5","yruiz","manzana123","Carniceria y roticeria");
        List<UReponedor> reponedorList = new ArrayList<>();
        reponedorList.add(reponedor1);
        reponedorList.add(reponedor2);
        reponedorList.add(reponedor3);
        return reponedorList;
    }
    @GetMapping("cajero")
    public List<UCajero> listaCajeros(){
        UCajero cajero1 = new UCajero("Ricardo Jimenez","12458369-1","250-RicardoJ","520Toam","Caja 1");
        UCajero cajero2 = new UCajero("Fernando Ferrer","12458889-1","190-FernandoF","420Team","Caja 2");
        UCajero cajero3 = new UCajero("Fernando Tabie","12336842-2","110-FernandoT","550Serena","Caja 3");
        UCajero cajero4 = new UCajero("Graciela Perez","19458429-9","190-GracielaP","480Darth","Caja 4");
        UCajero cajero5 = new UCajero("Alicia Sepulveda","20251497-k","190-AliciaS","590seraphine","Caja 5");

        List<UCajero> cajerosList = new ArrayList<>();
        cajerosList.add(cajero1);
        cajerosList.add(cajero2);
        cajerosList.add(cajero3);
        cajerosList.add(cajero4);
        cajerosList.add(cajero5);
        return cajerosList;
    }
    @GetMapping("merma")
    public List<Merma> listaMerma(){
        Merma merma1 = new Merma("Tomates 20-02-22","Verduras en mal estado por gusanos",20);
        Merma merma2 = new Merma("Huevos 20-02-22","Alimento perecible en mal estado",12);
        Merma merma3 = new Merma("Huevos 25-02-22","Alimento perecible en mal estado",30);

        List<Merma> mermaList = new ArrayList<>();
        mermaList.add(merma1);
        mermaList.add(merma2);
        mermaList.add(merma3);
        return mermaList;
    }
}
